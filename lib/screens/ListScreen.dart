
import 'package:flutter/material.dart';
import 'package:mailapp/constants.dart';
import 'package:mailapp/model/backend.dart';
import 'package:mailapp/model/email.dart';
import 'package:mailapp/screens/DetailScreen.dart';

class ListScreen extends StatefulWidget {
  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  List<Email> emails = Backend().getEmails();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Mock mail'),
          centerTitle: true,
        ),
        body: ListView.builder(
            itemCount: emails.length,
            itemBuilder: (BuildContext context, int index) {
              final email = emails[index];
              final id = email.id;
              final bool read = email.read;
              return Dismissible(
                key: ValueKey(id),

                child: InkWell(
                  /*onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context)=> DetailScreen(email: email),
                      ) 
                    );
                  }, */
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                    height: 100.0,
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: CircleWidget(radius: 10),
                          //child: Container(
                          //height: 12.0,
                          //decoration: BoxDecoration(
                          //shape: BoxShape.circle,
                          //color: Colors.amberAccent),

                            //),
                        ),
                        Expanded(
                          flex: 5,
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: Colors.blueAccent, width: 1.0
                                ),
                              ), 
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                  
                                Text(email.dateTime.toString().substring(0, 10),
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                                Text(email.from,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(email.subject),
                              ],
                              
                            ),
                          ),
                             
                        ),
                      ],
                    ),  
                  ),
                  onLongPress: (){
                    Backend().markEmailAsRead(id);
                    setState(() {});
                  },
                  onTap: (){
                    Backend().markEmailAsRead(id);
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context)=> DetailScreen(email: emails[index],
                      )));
                    setState(() {});
                  },  
                ),
                onDismissed: (direction){
                  setState(() {
                    emails.removeAt(index);
                    Backend().deleteEmail(id);
                    print(toString());
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      duration: Duration(seconds: 1),
                      content:Text("$id")));
                      setState(() {});
                      //context))
                  });
                },
              );
            },
        ),
    );
  }
}
