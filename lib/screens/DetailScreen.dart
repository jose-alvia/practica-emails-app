import 'package:flutter/material.dart';
import 'package:mailapp/model/email.dart';



class DetailScreen extends StatefulWidget {
  final Email email;
  const DetailScreen({ Key? key, required this.email }) : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    final email = widget.email;
    return Scaffold(
      appBar: AppBar(
        title: Text(email.subject)
      ),      
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 15, left: 10),
                child: Text(
                  "From: ${email.from}",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
            ],
          ),
          Divider(
            color: Colors.blueAccent,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                  padding: EdgeInsets.only(left: 10),              
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(email.subject),
                    ],
                ),
              ),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Text(email.dateTime.toString(),
                        style: TextStyle(fontSize: 10)),
                  ),
                ], 
              )
            ],
          ),
          Divider(
            color: Colors.blueAccent,
          ),
          Padding(
            padding: EdgeInsets.only(top: 3, left: 10, right: 10),
            child: Row(
              children: [
                Flexible(
                  flex: 1,
                  child: Text(
                    email.body,
                    textAlign: TextAlign.justify,
                  ),
                ),
              ]),
          )

        ]),
    );
  }
}

