import 'package:flutter/material.dart';
import 'package:mailapp/constants.dart';
import 'package:mailapp/model/email.dart';
import 'package:mailapp/model/backend.dart';
import 'package:mailapp/screens/DetailScreen.dart';



class EmailWidget extends StatelessWidget {
  final Email email;


  const EmailWidget({Key? key, required this.email}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      //margin EdgeInsets.all(20.0),
      child: InkWell(
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context)=> DetailScreen(email: email),
            ) 
          );
        }, 
        child: Container(
          padding: const EdgeInsets.all(10.0),
          height: 100.0,
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: CircleWidget(radius: 10),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.blueAccent, width: 1.0
                      ),
                    ), 
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                  
                      Text(email.dateTime.toString().substring(0, 10),
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      Text(email.from,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(email.subject),
                    ],
                  ),
                ),   
              ),
            ],
          ),  
        ),
      ),
    );
  }
}

