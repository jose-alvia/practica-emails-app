import 'package:flutter/material.dart';
import 'package:mailapp/model/backend.dart';
import 'package:mailapp/screens/ListScreen.dart';
import 'package:mailapp/widgets/email_widget.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      //home: const MyHomePage(title: 'mock mail'),
      home: ListScreen(),
    );
  }
}

/*class MyHomePage extends StatefulWidget {
  const MyHomePage({ Key? key, required this.title }) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final emails =Backend().getEmails();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),

      body: Center(
        child: ListView(
          children: emails
              .map((email) => EmailWidget(email: email))
              .toList()

        ),
      ),
      
    );
  }
}*/





