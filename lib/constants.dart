import 'package:flutter/material.dart';

class CircleWidget extends StatelessWidget {
  final double radius;
  const CircleWidget({ Key? key, required this.radius }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: radius,
      height: radius,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.blueAccent
      ),
      
    );
  }
}